# dither-binaries
Releases for [dither](https://crates.io/crates/dither)

* [crate](https://crates.io/crates/dither)
* [documentation](https://docs.rs/dither/1.3.4/dither/)
* [repository](https://gitlab.com/efronlicht/dither)
* [binaries](https://gitlab.com/efronlicht/dither-binaries)
* [dockerfile](https://hub.docker.com/r/efronlicht/dither)

## Latest Version: 1.3.4

## License: MIT
